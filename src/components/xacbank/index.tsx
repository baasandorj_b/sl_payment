import { Card } from "antd";
import React from "react";
import xac_logo from "../../images/logos/xac_logo.png";

export default () => {
  return (
    <div>
      <Card style={{ textAlign: "center" }}>
        <img src={xac_logo} height="50px" />
      </Card>
    </div>
  );
};
