import { Card, Spin } from "antd";
import React from "react";
import { createInvoice } from "../../API/restAPI";
import monpay from "../../images/logos/monpay.png";
interface IProps {
  invoiceId: string;
  endpoint: string;
  apiKey: string;
  username: string;
}
export default (props: IProps) => {
  const [visible, setVisible] = React.useState(false);
  const { data, loading, action } = createInvoice("monpay");
  const onClick = () => {
    action(props.endpoint, props.apiKey, props.invoiceId, props.username);
  };
  React.useEffect(() => {
    if (data) {
      console.log(data);
      setVisible(true);
    }
  }, [data]);
  return (
    <Spin spinning={loading}>
      <Card style={{ textAlign: "center" }} onClick={onClick}>
        <img src={monpay} height="50px" />
      </Card>
    </Spin>
  );
};
