import typescript from "rollup-plugin-typescript2";
import pkg from "./package.json";
import postcss from "rollup-plugin-postcss";
import image from "rollup-plugin-img"
export default {
  input: "src/index.tsx",
  output: [
    {
      file: pkg.main,
      format: "cjs",
    },  
    {
      file: pkg.module,
      format: "es",
    },
 
  ],
  external: [
    ...Object.keys(pkg.dependencies || {}),
    ...Object.keys(pkg.peerDependencies || {}),
  ],
  plugins: [
    typescript({
      typescript: require("typescript"),
    }),
    postcss({
        use: {               
            less: { javascriptEnabled: true }
        },
        extensions: ['.less'], // Looks like this still processes CSS despite this line.
        extract: false ,
       
    }),    
    image({
      limit: 500000000
    }),
  ],
};
