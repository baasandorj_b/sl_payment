import Modal from "antd/lib/modal/Modal";
import React from "react";
interface IProps {
  visible: boolean;
  setVisible: (visible: boolean) => void;
  data: any;
}
export default (props: IProps) => {
  return (
    <Modal
      visible={props.visible}
      onCancel={() => props.setVisible(false)}
      footer={false}
      width={300}
      title="MONPAY"
      centered
    >
      <div style={{ textAlign: "center" }}>
        <img
          style={{ width: "100%", height: "auto" }}
          src={"data:image/jpeg;base64," + props?.data?.qrImage}
        />
      </div>
    </Modal>
  );
};
