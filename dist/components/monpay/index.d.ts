/// <reference types="react" />
interface IProps {
    invoiceId: string;
    endpoint: string;
    apiKey: string;
    username: string;
}
declare const _default: (props: IProps) => JSX.Element;
export default _default;
