import React, { CSSProperties } from "react";
import { Button, Card, Col, Row, Skeleton } from "antd";
import Qpay from "./components/qpay/index";
import XacCard from "./components/xacbank/index";
import Monpay from "./components/monpay/index";
import "./styles/main.less";
import { invoiceInput, registerUser, createUserInvoice } from "./API/restAPI";

interface IProps {
  endpoint: string;
  username: string;
  apiKey: string;
  invoiceInput: invoiceInput;
}

const App = (props: IProps) => {
  const [loading, setLoading] = React.useState(true);
  const newUser = registerUser();
  const userInvoice = createUserInvoice();
  React.useEffect(() => {
    if (
      props.endpoint &&
      props.apiKey &&
      props.username &&
      props.invoiceInput
    ) {
      setLoading(false);
      newUser.action(props.endpoint, props.apiKey, props.username);
    }
  }, [props.endpoint, props.username, props.apiKey]);
  React.useEffect(() => {
    if (
      newUser?.data?.status === "success" ||
      newUser?.data?.status === "already registred"
    ) {
      userInvoice.action(
        props.endpoint,
        props.apiKey,
        props.username,
        props.invoiceInput
      );
    }
  }, [newUser?.data]);
  return (
    <Skeleton
      loading={newUser.loading || userInvoice.loading || loading}
      active
    >
      <Row gutter={24}>
        <Col md={8} sm={12} xs={24}>
          <Qpay
            invoiceId={userInvoice?.data?.id}
            endpoint={props.endpoint}
            apiKey={props.apiKey}
            username={props.username}
          />
        </Col>
        <Col md={8} sm={12} xs={24}>
          <Monpay
            invoiceId={userInvoice?.data?.id}
            endpoint={props.endpoint}
            apiKey={props.apiKey}
            username={props.username}
          />
        </Col>
        <Col md={8} sm={12} xs={24}>
          <XacCard />
        </Col>
      </Row>
    </Skeleton>
  );
};

export default App;
