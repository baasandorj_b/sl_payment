/// <reference types="react" />
interface IProps {
    visible: boolean;
    setVisible: (visible: boolean) => void;
    data: any;
}
declare const _default: (props: IProps) => JSX.Element;
export default _default;
