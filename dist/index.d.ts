/// <reference types="react" />
import "./styles/main.less";
import { invoiceInput } from "./API/restAPI";
interface IProps {
    endpoint: string;
    username: string;
    apiKey: string;
    invoiceInput: invoiceInput;
}
declare const App: (props: IProps) => JSX.Element;
export default App;
