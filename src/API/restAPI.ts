import React from "react";
import axios from "axios";

export interface invoiceInput {
  billNumber: string;
  amount: number;
  description: string;
  products: any;
  expireDate: Date;
}

export const signin = async (
  endpoint: string,
  apiKey: string,
  username: string
) => {
  const response = await axios({
    method: "GET",
    url: `${endpoint}/thirdparty/users/${username}/token`,
    headers: {
      Authorization: `Token ${apiKey}`,
    },
  });
  if (response?.data?.status === "success") {
    return response?.data;
  }
};

export const createUserInvoice = () => {
  const [data, setData] = React.useState<any>();
  const [loading, setLoading] = React.useState(false);
  const action = async (
    endpoint: string,
    apiKey: string,
    username: string,
    invoiceInput: invoiceInput
  ) => {
    setLoading(true);
    axios({
      url: `${endpoint}/thirdparty/users/${username}/invoices`,
      method: "POST",
      headers: {
        Authorization: `Token ${apiKey}`,
      },
      data: {
        ...invoiceInput,
      },
    })
      .then((res) => {
        setData(res?.data?.data);
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return { data, loading, action };
};

export const registerUser = () => {
  const [data, setData] = React.useState<any>();
  const [loading, setLoading] = React.useState(false);
  const action = (endpoint: string, apiKey: string, username: string) => {
    setLoading(true);
    axios({
      url: `${endpoint}/thirdparty/users`,
      method: "POST",
      headers: {
        Authorization: `Token ${apiKey}`,
      },
      data: {
        username,
      },
    })
      .then((res) => {
        setData(res?.data);
      })
      .catch((err) => {
        if (err?.response?.data?.message === "User exists.") {
          setData({ status: "already registred" });
        } else {
          console.log(err);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return { data, loading, action };
};
export const createInvoice = (method: "qpay" | "monpay" | "xac" | "golomt") => {
  const [data, setData] = React.useState<any>();
  const [loading, setLoading] = React.useState(false);
  const action = async (
    endpoint: string,
    apiKey: string,
    invoiceId: string,
    username: string
  ) => {
    setLoading(true);
    const authResponse = await signin(endpoint, apiKey, username);
    axios({
      url: `${endpoint}/sdk/invoices/${invoiceId}/${method}`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${authResponse?.data?.accessToken}`,
      },
      data: {
        invoiceId,
      },
    })
      .then((res) => {
        setData(res?.data?.data);
      })
      .catch((err) => {
        console.error(err);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return { data, loading, action };
};
