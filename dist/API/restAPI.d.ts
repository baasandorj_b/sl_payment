export interface invoiceInput {
    billNumber: string;
    amount: number;
    description: string;
    products: any;
    expireDate: Date;
}
export declare const signin: (endpoint: string, apiKey: string, username: string) => Promise<any>;
export declare const createUserInvoice: () => {
    data: any;
    loading: boolean;
    action: (endpoint: string, apiKey: string, username: string, invoiceInput: invoiceInput) => Promise<void>;
};
export declare const registerUser: () => {
    data: any;
    loading: boolean;
    action: (endpoint: string, apiKey: string, username: string) => void;
};
export declare const createInvoice: (method: "qpay" | "monpay" | "xac" | "golomt") => {
    data: any;
    loading: boolean;
    action: (endpoint: string, apiKey: string, invoiceId: string, username: string) => Promise<void>;
};
