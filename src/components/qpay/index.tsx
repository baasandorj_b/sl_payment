import { Card, Skeleton, Spin } from "antd";
import React from "react";
import qpay_logo from "../../images/logos/qpay_logo.png";
import { createInvoice } from "../../API/restAPI";
import Modal from "./modal";
interface IProps {
  invoiceId: string;
  endpoint: string;
  apiKey: string;
  username: string;
}

export default (props: IProps) => {
  const [visible, setVisible] = React.useState(false);
  const { data, loading, action } = createInvoice("qpay");
  const onClick = () => {
    action(props.endpoint, props.apiKey, props.invoiceId, props.username);
  };
  React.useEffect(() => {
    if (data) {
      console.log(data);
      setVisible(true);
    }
  }, [data]);
  return (
    <>
      <Spin spinning={loading}>
        <Card style={{ textAlign: "center" }} onClick={onClick}>
          <img src={qpay_logo} height="50px" />
        </Card>
      </Spin>
      <Modal visible={visible} setVisible={setVisible} data={data} />
    </>
  );
};
